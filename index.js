const message = document.getElementById('message');
let rotateDeg = 0;

setInterval(() => {
  rotateDeg += 5;
  message.style.transform = `rotate(${rotateDeg}deg)`;
}, 50);
